let initialMessage = 'Este programa permite al usuario hacer un pedido en un restaurante de comida rápida.\n\n'
initialMessage += 'Si se agrega el mismo pedido se actualizará la cantidad del mismo pedido.\n\n'
initialMessage += 'Se tienen tres intentos para ingresar un pedido correcto. Al terminar los tres intentos se cerrará la orden con los pedidos ingresados.'

alert(initialMessage)

function Option(id, name, price) {
    this.id = id
    this.name = name
    this.price = price

    this.text = function () {
        return this.id + ' - ' + this.name + ' | ' + this.priceText()
    }

    this.priceText = function () {
        return `S/ ${this.price.toFixed(2)}`
    }
}

function SelectedOption(option, quantity) {
    this.option = option
    this.quantity = quantity

    this.subtotal = function () {
        return quantity * this.option.price
    }

    this.subtotalText = function () {
        return `S/ ${this.subtotal().toFixed(2)}`
    }

    this.priceText = function () {
        return option.priceText()
    }
}

const menu = [
    new Option(1, '1/4 Pollo a la brasa', 18.00),
    new Option(2, '1/2 Pollo a la brasa', 35.00),
    new Option(3, '1 Pollo a la brasa', 65.00),
    new Option(4, 'Salchipapa', 10.00),
    new Option(5, 'Broaster', 15.00),
    new Option(6, 'Anticuchos', 20.00),
    new Option(7, 'Parrilla', 80.00),
    new Option(8, 'Arroz Chaufa', 12.00),
]

let selectedOptions = []

function init() {

    let message = ''
    let more = true
    let total = 0

    do {
        let option = getOption()
        if (!option) {
            break
        }

        let quantity = getQuantityValue()
        if (quantity === null) {
            break
        }

        const optionSelected = getSelectedOptionById(option.id)
        if (optionSelected) {
            updateQuantity(optionSelected.option, quantity)
        } else {
            selectedOptions.push(new SelectedOption(option, quantity))
        }

        more = confirm('¿Desea agregar más opciones?')
    } while (more)

    selectedOptions.forEach(function (selectedOption) {
        total += selectedOption.subtotal()
        message += `Opción: ${selectedOption.option.name}\nPrecio: ${selectedOption.priceText()}\nCantidad: ${selectedOption.quantity}\nSubtotal: ${selectedOption.subtotalText()}\n\n`
    })

    if (message === '') {
        message = 'No se agregaron opciones a la orden'
    } else {
        message += `-------------------------------\nTOTAL: S/ ${total.toFixed(2)}`
    }

    alert(message)
    console.log(message)
}

function getOption() {
    const message = getOptionsText() + '\n\n' + 'Selecciona una opción de la lista:'
    let attempts = 3


    while (attempts > 0) {
        let menuOption = parseInt(prompt(message))
        if (typeof menuOption === 'number' && !Number.isNaN(menuOption)) {
            const option = getOptionById(menuOption)
            if (option) {
                return option
            }
        }

        attempts--

        if (attempts === 0) {
            alert('Se superaron los intentos disponible.')
        } else {
            alert(`El valor seleccionado no es correcto. Intentos disponibles: ${attempts}`)
        }
    }

    return null
}

function getQuantityValue() {
    let attempts = 3

    while (attempts > 0) {
        let quantity = parseInt(prompt('Ingresa la cantidad'))
        if (typeof quantity === 'number' && !Number.isNaN(quantity)) {
            return quantity
        }

        attempts--

        if (attempts === 0) {
            alert('Se superaron los intentos disponible.')
        } else {
            alert(`El valor ingresado no es correcto. Intentos disponibles: ${attempts}`)
        }
    }

    return null
}

function getOptionsText() {
    return menu.map(option => option.text()).join('\n')
}

function getOptionById(id) {
    return menu.find(option => option.id === id)
}

function getSelectedOptionById(id) {
    return selectedOptions.find(selectedOption => selectedOption.option.id === id)
}

function updateQuantity(option, newQuantity) {
    const foundOptionIndex = selectedOptions.findIndex(selectedOption => selectedOption.option === option)
    if (foundOptionIndex !== -1) {
        selectedOptions[foundOptionIndex].quantity = newQuantity
    }
}

init()
